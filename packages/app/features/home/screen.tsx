import {
  Anchor,
  Button,
  H1,
  Paragraph,
  Separator,
  Sheet,
  useToastController,
  XStack,
  YStack,
} from '@my/ui'
import { ChevronDown, ChevronUp } from '@tamagui/lucide-icons'
import React, { useState } from 'react'
import { SolitoImage } from 'solito/image'
import { useLink } from 'solito/link'

export function HomeScreen() {
  const github = require('../../assets/GitHub-Desktop-Logo.webp')
  const gitlab = require('../../assets/gitlab.png')
  return (
    <YStack f={1} jc="center" ai="center" p="$4" space>
      <YStack space="$4" maw={600}>
        <XStack space="$4" ai="center" jc="space-between">
          <SolitoImage src={gitlab} alt="gitlab" width={200} height={200} />
          <SolitoImage src={github} alt="github" width={200} height={200} />
        </XStack>
        <H1 ta="center">Welcome HK..!</H1>
        <Paragraph ta="center">
          Hope you are having a great day! This is a demo app for{' '}
          <Anchor color="$color12" href="https://gitlab.com/Harikittu46" target="_blank">
            HK Portfolio
          </Anchor>
        </Paragraph>

        <Separator />
        <Paragraph ta="center">
          Made by{' '}
          <Anchor color="$color12" href="https://gitlab.com/Harikittu46" target="_blank">
            @Hari Krishna Setti
          </Anchor>
          ,{' '}
          <Anchor
            color="$color12"
            href="https://gitlab.com/Harikittu46"
            target="_blank"
            rel="noreferrer"
          >
            give it a ⭐️
          </Anchor>
        </Paragraph>
      </YStack>

      <SheetDemo />
    </YStack>
  )
}

function SheetDemo() {
  const [open, setOpen] = useState(false)
  const [position, setPosition] = useState(0)
  const toast = useToastController()

  return (
    <>
      <Button
        size="$6"
        icon={open ? ChevronDown : ChevronUp}
        circular
        onPress={() => setOpen((x) => !x)}
      />
      <Sheet
        modal
        animation="lazy"
        open={open}
        onOpenChange={setOpen}
        snapPoints={[80]}
        position={position}
        onPositionChange={setPosition}
        dismissOnSnapToBottom
      >
        <Sheet.Overlay animation="lazy" enterStyle={{ opacity: 0 }} exitStyle={{ opacity: 0 }} />
        <Sheet.Frame ai="center" jc="center">
          <Sheet.Handle />
          <Button
            size="$6"
            circular
            icon={ChevronDown}
            onPress={() => {
              setOpen(false)
              toast.show('Sheet closed!', {
                message: 'Just showing how toast works...',
              })
            }}
          />
        </Sheet.Frame>
      </Sheet>
    </>
  )
}
